import _ from 'lodash';
import fs from 'fs';
import path from 'path';
import http from 'http';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import microtime from 'microtime';
import merge from 'deepmerge';
import uniqid from 'uniqid';
import { expressCspHeader, SELF } from 'express-csp-header';
import helmet from 'helmet';
import timeout from 'connect-timeout';
import compress from 'compression';
import cors from 'cors';
import httpContext from 'express-cls-hooked';
import swaggerUi from 'swagger-ui-express';
import jsyaml from 'js-yaml';
import * as OpenApiValidator from 'express-openapi-validator';
import { OpenApiSpecLoader } from 'express-openapi-validator/dist/framework/openapi.spec.loader';
import SocketIo from 'socket.io';
import redisAdapter from '@socket.io/redis-adapter';
import rateLimit from 'express-rate-limit';
import routeHandlerModule from './routeHandler';
import elasticsearchTransport from './transport/elasticsearch';
import consoleTransport from './transport/console';
import identityServiceClient from './clients/identityService';
import loggerModule from './logger';
import httpClientModule from './httpClient';
import config from './config';
import ContextClass from './lib/Context';

class RouteNotFoundError extends Error {
  constructor(message = 'Route does not exist.') {
    super(message);
    Error.captureStackTrace && Error.captureStackTrace(this, RouteNotFoundError); // eslint-disable-line no-unused-expressions
    this.status = 404;
    this.statusCode = 404;
  }
}

const clientInstances = {
  'identity-service-v1': identityServiceClient
};

const noopMiddleware = (req, res, next) => next();

export const Context = ContextClass;
export { default as ServiceEnum } from './enum/ServiceEnum';

export default async (serviceId, container, opts = {}) => {
  const apiOpts = {
    apiSpecFile: opts.apiSpecFile || path.join(process.cwd(), process.env.NODE_PATH, 'api.yaml'),
    securityHandlers: opts.securityHandlers || {},
    validateRequests: !!opts.validateRequests,
    validateResponses: !!opts.validateResponses,
    logger: {
      logRoutes: opts.logger ? !!opts.logger.logRoutes : false,
      logQueries: opts.logger ? !!opts.logger.logQueries : false,
      transport: opts.logger && opts.logger.transport ? opts.logger.transport : 'console',
    },
    jwts: _.isArray(opts.jwts) ? opts.jwts.map((jwt) => {
      if (!(jwt.header && jwt.assignTo && jwt.secret)) {
        throw new Error('JWT misconfiguration - required attributes: {header, assignTo, secret}');
      }

      return jwt;
    }) : [],
    ws: opts.ws ? {
      redisDsn: opts.ws.redisDsn || null,
      cors: opts.ws.cors || {
        origin: '*',
        methods: ['GET', 'POST'],
        transports: ['websocket', 'polling'],
        credentials: true
      },
      allowEIO3: opts.ws.allowEIO3 || false,
      pingTimeout: opts.ws.pingTimeout || 30000,
      pingInterval: opts.ws.pingInterval || 3000
      // ... other socket.io opts
    } : null,
    clientsFile: opts.clientsFile || path.join(process.cwd(), process.env.NODE_PATH, 'clients.json'),
  };

  let clients = {};
  if (fs.existsSync(apiOpts.clientsFile)) {
    clients = require(apiOpts.clientsFile); // eslint-disable-line
    const overrideFile = apiOpts.clientsFile.replace('.json', '.override.json');
    if (fs.existsSync(overrideFile)) {
      const override = require(overrideFile); // eslint-disable-line
      clients = merge(clients, override);
    }
  }

  apiOpts.clients = Object.entries(clients).map((entry) => {
    if (!clientInstances[entry[0]]) {
      throw new Error(`Unknown client ID: ${entry[0]}`);
    }

    return {
      id: entry[0],
      ...entry[1]
    };
  });

  const { elasticsearch } = config.logger.transport;
  const transport = apiOpts.logger.transport === 'console' ? consoleTransport(serviceId) : elasticsearchTransport(serviceId, elasticsearch);
  const logger = loggerModule(transport, apiOpts.logger.logRoutes, apiOpts.logger.logQueries);
  const httpClient = httpClientModule(serviceId);

  if (container.has('logger') && !['test', 'test-api'].includes(process.env.NODE_ENV)) {
    throw new Error('Cannot inject internal module "logger", the name is already taken in container. Rename the module.');
  }

  if (!container.has('logger')) {
    container.register('logger', () => logger);
  }

  if (container.has('Context') && !['test', 'test-api'].includes(process.env.NODE_ENV)) {
    throw new Error('Cannot inject internal module "logger", the name is already taken in container. Rename the module.');
  }

  if (!container.has('Context')) {
    container.register('Context', () => ContextClass);
  }

  if (container.has('httpClient') && !['test', 'test-api'].includes(process.env.NODE_ENV)) {
    throw new Error('Cannot inject internal module "httpClient", the name is already taken in container. Rename the module.');
  }

  if (!container.has('httpClient')) {
    container.register('httpClient', () => httpClient);
  }

  apiOpts.clients.forEach((client) => {
    const clientModuleName = client.as;

    if (container.has(clientModuleName) && !['test', 'test-api'].includes(process.env.NODE_ENV)) {
      throw new Error(`Cannot inject internal module "${clientModuleName}", the name is already taken in container. Rename the module.`);
    }

    if (!container.has(clientModuleName)) {
      container.register(clientModuleName, () => clientInstances[client.id](httpClientModule(serviceId, client.baseUrl)));
    }
  });

  const apiSpec = jsyaml.load(fs.readFileSync(apiOpts.apiSpecFile));
  const app = express();

  app.use(express.json({ limit: '5000mb' }));
  app.use(express.urlencoded({ limit: '5000mb', extended: true, parameterLimit: 5000000 }));
  app.use(bodyParser.json({ limit: '5000mb' }));
  app.use(bodyParser.urlencoded({ limit: '5000mb', extended: true, parameterLimit: 5000000 }));

  app.use(compress());

  app.use(bodyParser.json());
  app.use(bodyParser.text());
  app.use(cookieParser());

  // default headers
  if (opts.cors !== false) {
    app.use(cors());
  }

  app.use((req, res, next) => {
    req.__request_start = microtime.now();
    next();
  });

  app.use(httpContext.middleware);
  app.use(timeout('20m')); // 20 minutes

  app.use((req, res, next) => {
    httpContext.set('httpHeaders', req.headers);
    httpContext.set('correlationId', req.headers['x-correlation-id'] || uniqid());
    next();
  });

  app.disable('x-powered-by');

  app.use(expressCspHeader({
    policies: {
      'script-src': [SELF]
    }
  }));

  app.use(helmet.frameguard());

  if (process.env.NODE_ENV !== 'production') {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(apiSpec));
  }

  const securityHandlerFns = _.mapValues(apiOpts.securityHandlers, (definition) => {
    if (!container.has(definition.service)) {
      throw new Error(`Invalid security handler definition, service ${definition.service} does not exist`);
    }

    const service = container.get(definition.service);
    if (typeof service[definition.method] !== 'function') {
      throw new Error(`Invalid security handler definition, method ${definition.method} of ${definition.service} does not exist`);
    }

    return service[definition.method];
  });

  const securityHandlers = _.mapValues(securityHandlerFns, (fn) => async (req) => {
    try {
      await fn(req);
      return true;
    } catch (err) {
      err.correlationId = httpContext.get('correlationId');
      throw err;
    }
  });

  app.use(OpenApiValidator.middleware({
    apiSpec: apiOpts.apiSpecFile,
    validateRequests: apiOpts.validateRequests,
    validateResponses: apiOpts.validateResponses,
    validateSecurity: {
      handlers: securityHandlers
    }
  }));

  const routeHandler = routeHandlerModule(logger, apiOpts.jwts, apiOpts.validateResponses);

  const specLoader = new OpenApiSpecLoader({
    apiDoc: apiOpts.apiSpecFile,
  });

  const spec = await specLoader.load();
  const { paths } = spec.apiDoc;
  spec.routes.forEach((route) => {
    const { method, expressRoute } = route;
    const openApiRoute = route.openApiRoute.replace(route.basePath, '').trim();
    if (paths[openApiRoute] && paths[openApiRoute][method.toLowerCase()]) {
      const schema = paths[openApiRoute][method.toLowerCase()];
      const oId = schema['x-eov-operation-id'];
      const oHandler = schema['x-eov-operation-handler'];

      const handlers = container.get(`${oHandler}Handlers`);
      if (typeof handlers[oId] !== 'function') {
        throw new Error(`Invalid or missing opration handler. Route: ${method} ${openApiRoute}, handler: ${oHandler}, operation: ${oId}`);
      }

      const limiterDuration = schema['x-limiter-duration'];
      const limiterMax = schema['x-limiter-max-requests'];
      const limiterEnabled = limiterDuration && limiterMax;
      const limiterMiddleware = limiterEnabled ? rateLimit({
        windowMs: limiterDuration,
        max: limiterMax,
        message: {
          status: 429,
          path: expressRoute,
          message: 'Too Many Requests'
        }
      }) : noopMiddleware;

      app[method.toLowerCase()](expressRoute, limiterMiddleware, (req, res) => routeHandler.handleRoute(req, res, handlers[oId]));
    } else {
      throw new Error(`Cannot find schema definition for endpoint: ${method} ${openApiRoute}`);
    }
  });

  container.register('app', () => app);

  const server = new http.Server(app);

  if (apiOpts.ws) {
    const { redisDsn, ...wsOpts } = apiOpts.ws;
    const ws = SocketIo(server, wsOpts);
    if (redisDsn) {
      ws.adapter(redisAdapter(redisDsn));
    }

    app.ws = ws;
    if (container.has('socketIo') && !['test', 'test-api'].includes(process.env.NODE_ENV)) {
      throw new Error('Cannot inject internal module "socketIo", the name is already taken in container. Rename the module.');
    }

    container.register('socketIo', () => ws);
  }

  return {
    run(port, netInterface = '0.0.0.0', done = () => {}) {
      // 404 handler
      app.use('*', (req, res) => routeHandler.handleError(new RouteNotFoundError(), req, res));

      // error handler
      app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
        routeHandler.handleError(err, req, res);
      });

      server.listen(port, netInterface, () => {
        console.log('API server is listening on %s:%s', netInterface, port); // eslint-disable-line no-console
        done();
      });

      return server;
    },
    get(...args) {
      return app.get(...args);
    },
    post(...args) {
      return app.post(...args);
    },
    put(...args) {
      return app.put(...args);
    },
    patch(...args) {
      return app.patch(...args);
    },
    delete(...args) {
      return app.delete(...args);
    },
    options(...args) {
      return app.options(...args);
    },
    use(...args) {
      return app.use(...args);
    },
    override(moduleName, moduleFactory) {
      container.override(moduleName, moduleFactory);
    },
  };
};
