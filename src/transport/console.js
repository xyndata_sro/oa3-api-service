/* eslint-disable no-console */
import circularJson from 'circular-json';

export default (serviceId) => ({
  log(correlationId, ...args) {
    console.log(`LOG [${serviceId}: ${correlationId}]`, ...args);
  },
  warning(correlationId, ...args) {
    console.log(`WARNING [${serviceId}: ${correlationId}]`, ...args);
  },
  error(correlationId, ...args) {
    console.log(`ERROR [${serviceId}: ${correlationId}]`, ...args);
  },
  logQuery(correlationId, query, params, stats) {
    const logData = {
      query: query.replace(/[\n\t]+/g, '').replace(/[ ]+/g, ' ').replace(/\([ ]+/g, '(').replace(/[ ]+\)/g, ')').trim(), // eslint-disable-line
      params,
      ...stats,
      timestamp: new Date()
    };

    console.log(`QUERY [${serviceId}: ${correlationId}]`, JSON.stringify(logData, null, 2));
  },
  logRoute(correlationId, method, route, data, took, error) {
    const logData = {
      method: method ? method.toUpperCase() : null,
      route,
      data,
      took,
      errorCode: error && error.status ? error.status : null,
      error,
      timestamp: new Date()
    };

    console.log(`ROUTE [${serviceId}: ${correlationId}]`, circularJson.stringify(logData, null, 2));
  }
});
