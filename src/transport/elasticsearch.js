import _ from 'lodash';
import dbg from 'debug';
import crypto from 'crypto';
import circularJson from 'circular-json';
import elasticsearch from 'elasticsearch';

const debug = dbg('oa3-api-service:logger:elasticsearch');

export default (serviceId, config) => {
  const client = new elasticsearch.Client({
    host: `${config.host}:${config.port}`,
    log: config.logLevel || 'error',
    auth: `${config.username}:${config.password}`
  });

  _.map(config.indices, async (index) => {
    const exists = await client.indices.exists({ index: index.index });
    if (!exists) {
      debug(`Index ${index.index} does not exist, creating`);
      await client.indices.create({
        index: index.index,
        body: index.definition
      });
    } else {
      debug(`Index ${index.index} exists`);
    }
  });

  const indexData = (data) => {
    client.index(data).catch(e => console.error('Elastic call has failed. Data:', data, 'Error:', e)); // eslint-disable-line
  };

  const log = (correlationId, level, ...args) => {
    indexData({
      index: config.indices.common.index,
      type: config.indices.common.type,
      body: {
        serviceId,
        correlationId,
        level,
        ...args,
        '@timestamp': new Date()
      }
    });
  };

  return {
    log(correlationId, ...args) {
      log(correlationId, 'log', ...args);
    },
    warning(correlationId, ...args) {
      log(correlationId, 'warning', ...args);
    },
    error(correlationId, ...args) {
      log(correlationId, 'error', ...args);
    },
    logQuery(correlationId, query, params, stats) {
      indexData({
        index: config.indices.query.index,
        type: config.indices.query.type,
        body: {
          serviceId,
          correlationId,
          hash: crypto.createHash('md5').update(query).digest('hex'),
          query: query.replace(/[\n\t]+/g, '').replace(/[ ]+/g, ' ').replace(/\([ ]+/g, '(').replace(/[ ]+\)/g, ')').trim(), // eslint-disable-line
          params: JSON.stringify(params),
          ...stats,
          '@timestamp': new Date()
        }
      });
    },
    logRoute(correlationId, method, route, data, took, error) {
      indexData({
        index: config.indices.route.index,
        type: config.indices.route.type,
        body: {
          serviceId,
          correlationId,
          method: method ? method.toUpperCase() : null,
          route,
          data: JSON.stringify(data),
          took,
          errorCode: error && error.status ? error.status : null,
          error: error !== null ? circularJson.stringify(error) : null,
          '@timestamp': new Date()
        }
      });
    }
  };
};
