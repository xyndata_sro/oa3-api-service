import _ from 'lodash';
import jsonwebtoken from 'jsonwebtoken';
import httpContext from 'express-cls-hooked';
import isStream from 'is-stream';
import Context from './lib/Context';
import { UnauthorizedError } from './errors';

// const pino = require('pino');

export default (logger, jwts = [], reformatOutput = false) => {
  /* const loggerPino = pino({
    level: 'error',
    timestamp: pino.stdTimeFunctions.isoTime,
  }); */

  const decodeJwt = async (req, header, secret) => {
    const token = (req.headers[header] || '').trim().replace(/^bearer/i, '').trim();
    if (_.isEmpty(token)) {
      return null;
    }

    return new Promise((resolve, reject) => {
      jsonwebtoken.verify(token, secret, (err, decoded) => {
        if (err) {
          return reject(new UnauthorizedError(`Invalid token: ${header}`));
        }

        return resolve(decoded);
      });
    });
  };

  const handleError = (err, req, res) => {
    logger.logRoute(req, err);
    const payload = {
      status: err.status || 500,
      path: req.originalUrl,
      message: err.message || 'Internal server error',
      errors: err.errors || undefined,
      correlationId: httpContext.get('correlationId')
    };
    console.error('Route error', payload); // eslint-disable-line
    res.status(err.status || 500).json(payload);
  };

  return {
    handleError,
    async handleRoute(req, res, handlerFn) {
      try {
        await Promise.all(jwts.map(async (jwt) => {
          const decoded = await decodeJwt(req, jwt.header, jwt.secret);
          if (decoded !== null) {
            _.set(req, jwt.assignTo, decoded);
          }
        }));

        const ctx = new Context(req);
        const response = await handlerFn(req, ctx, res);
        logger.logRoute(req);

        if (isStream(response)) {
          response.on('error', (err) => {
            if (!(err.status || err.statusCode) && err.code) {
              err.status = err.code;
              err.statusCode = err.code;
            }
            handleError(err, req, res);
          });
          response.pipe(res);
          return;
        }

        if (_.isEmpty(response)) {
          res.end();
          return;
        }

        if (reformatOutput) {
          // needed for API tests, Date attributes are not properly coerced
          res.json(JSON.parse(JSON.stringify(response)));
          return;
        }

        res.json(response);
      } catch (err) {
        handleError(err, req, res);
      }
    }
  };
};
