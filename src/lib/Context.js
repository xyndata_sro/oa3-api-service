export default class Context {
  constructor(req) {
    this.jwt = req.jwt || {};
    const header = req.headers || [];
    this.caller = header['x-caller-service-id'];
    this.originCaller = header['x-origin-caller-service-id'];
    this.permission = header.permission;
  }

  getJwt() {
    return this.jwt;
  }

  getUserId() {
    return this.jwt.sub;
  }

  getUserCompanyId() {
    return this.jwt.aud;
  }

  getUserBrowserId() {
    return this.jwt.iss;
  }

  getUserPermissions() {
    return this.permission;
  }

  getCallerService() {
    return this.caller;
  }

  getOriginCallerService() {
    return this.originCaller;
  }
}
