export class UnauthorizedError extends Error {
  constructor(message = 'Unauthorized.') {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnauthorizedError);
    }
    this.status = 401;
    this.statusCode = 401;
  }
}

export default {
  UnauthorizedError
};
