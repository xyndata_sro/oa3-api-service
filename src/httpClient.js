import rp from 'request-promise';
import httpContext from 'express-cls-hooked';

export default (serviceId, baseUrl = null) => {
  const getRpInstance = () => {
    const { 'content-length': _ignored, host, connection, ...defaultHeaders } = httpContext.get('httpHeaders');
    return rp.defaults({
      baseUrl,
      headers: {
        ...defaultHeaders,
        'X-Correlation-Id': httpContext.get('correlationId'),
        'X-Caller-Service-Id': serviceId,
        'X-Origin-Caller-Service-Id': defaultHeaders['x-origin-caller-service-id'] || serviceId,
        'user-agent': `${serviceId}-http-agent`,
        'content-type': 'application/json'
      }
    });
  };

  function HttpClient(...args) {
    return getRpInstance()(...args);
  }

  ['get', 'post', 'put', 'patch', 'del', 'delete', 'head', 'options', 'cookie', 'defaults'].forEach((method) => {
    HttpClient[method] = (...methodArgs) => getRpInstance()[method](...methodArgs);
  });

  return HttpClient;
};
