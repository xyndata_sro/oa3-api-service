import microtime from 'microtime';
import httpContext from 'express-cls-hooked';

export default (transport, logRoutes = false, logQueries = false) => ({
  log(...args) {
    const correlationId = httpContext.get('correlationId');
    transport.log(correlationId, ...args);
  },
  warning(...args) {
    const correlationId = httpContext.get('correlationId');
    transport.warning(correlationId, ...args);
  },
  error(...args) {
    const correlationId = httpContext.get('correlationId');
    transport.error(correlationId, ...args);
  },
  logQuery(sql, params = [], stats = {}) {
    if (!logQueries) {
      return;
    }

    const correlationId = httpContext.get('correlationId');
    transport.logQuery(correlationId, sql, params, stats);
  },
  logRoute(req, error = null) {
    if (!logRoutes) {
      return;
    }

    const correlationId = httpContext.get('correlationId');
    const method = req.method ? req.method.toUpperCase() : null;
    const route = req.openapi ? req.openapi.openApiRoute : req.originalUrl;
    const data = {
      query: req.query,
      body: req.body,
      params: req.openapi ? req.openapi.pathParams : req.params,
      headers: req.headers
    };
    const took = req.__request_start ? microtime.now() - req.__request_start : -1;
    transport.logRoute(correlationId, method, route, data, took, error);
  }
});
