export default (httpClient) => ({
  /**
   *  Sample method to call identity service API
   */
  async getStatus() {
    return httpClient.get('/api/v1/status');
  },

  async createDefaultGroups(companyId) {
    return httpClient.post(`/api/v1/groups/create-default-groups/${companyId}`);
  },

  async getUserByUsername(username) {
    const filterUri = encodeURI(`{"username":{"$eq":"${username}"}}`);
    const response = await httpClient.get(`/api/v1/users?filter=${filterUri}`);
    return response && response.data[0];
  },

  async createUser(user) {
    const response = await httpClient({
      method: 'POST',
      uri: '/api/v1/users',
      body: {
        ...user,
      },
      json: true,
    });
    return response;
  },

  async createToken(userId, companyId, browserId) {
    const response = await httpClient({
      method: 'POST',
      uri: '/api/v1/user/create-token',
      body: {
        userId,
        companyId,
        browserId
      },
      json: true,
    });
    return response;
  }

});
