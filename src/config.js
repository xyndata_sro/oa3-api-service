module.exports = {
  logger: {
    transport: {
      elasticsearch: {
        host: process.env.ELASTICSEARCH_HOST || 'localhost',
        port: process.env.ELASTICSEARCH_PORT || 9200,
        logLevel: process.env.ELASTICSEARCH_LOG_LEVEL || 'error',
        username: process.env.ELASTICSEARCH_USERNAME || '',
        password: process.env.ELASTICSEARCH_PASSWORD || '',
        indices: {
          common: {
            index: 'api-service-log-common',
            type: 'log',
            definition: {
              mappings: {
                query: {
                  properties: {
                    serviceId: { type: 'keyword' },
                    correlationId: { type: 'keyword' },
                    level: { type: 'keyword' },
                    '@timestamp': { type: 'date' }
                  }
                }
              }
            }
          },
          query: {
            index: 'api-service-log-query',
            type: 'query',
            definition: {
              mappings: {
                query: {
                  properties: {
                    serviceId: { type: 'keyword' },
                    correlationId: { type: 'keyword' },
                    hash: { type: 'keyword' },
                    query: { type: 'text' },
                    params: { type: 'keyword' },
                    took: { type: 'integer' },
                    rows: { type: 'integer' },
                    alias: { type: 'keyword' },
                    failed: { type: 'boolean' },
                    '@timestamp': { type: 'date' }
                  }
                }
              }
            }
          },
          route: {
            index: 'api-service-log-route',
            type: 'route',
            definition: {
              mappings: {
                route: {
                  properties: {
                    serviceId: { type: 'keyword' },
                    correlationId: { type: 'keyword' },
                    method: { type: 'keyword' },
                    route: { type: 'keyword' },
                    took: { type: 'integer' },
                    errorCode: { type: 'integer' },
                    '@timestamp': { type: 'date' }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};
